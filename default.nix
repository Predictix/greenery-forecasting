{
  demand_forecasting_template_src ? ./.,
  retail_model_src ? ./.,
  demand_mgmt_src ? ./.,
  fm_ui ? ./.,
  nixpkgs ? <nixpkgs>,
  platform_release,
  app_dev_tools ? ../app-dev-tools,
  ...
}:

with import <config/lib> { inherit nixpkgs; };
let
  logicblox = getLB platform_release;
  retail_model = import retail_model_src { inherit nixpkgs platform_release; };
  demand_mgmt = import demand_mgmt_src { inherit nixpkgs platform_release retail_model_src fm_ui app_dev_tools; };
  demand_forecasting_template_version = version demand_forecasting_template_src;

in
  genericAppJobset {
    inherit logicblox;
    includeInstaller = false;
    build = buildLBConfig {
      name = "demand_forecasting_template-${demand_forecasting_template_version}";
      src = demand_forecasting_template_src;
      configureFlags = [ "--with-retail-model=${retail_model.build}" 
                         "--with-demand-mgmt=${demand_mgmt.build}"     
                       ];
      buildInputs = [ logicblox pkgs.utillinux pkgs.curl pkgs.python2 pkgs.python2Packages.pandas ];

      shellHook = ''
        export RETAIL_MODEL_HOME=${retail_model.build}
        export DEMAND_MGMT_HOME=${demand_mgmt.build}
      '';
    };
  }

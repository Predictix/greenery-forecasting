import argparse
import csv

parser = argparse.ArgumentParser(description="Demand forecast template : process customer data")
parser.add_argument("--settings_file", help="Settings file", type=str, nargs='?', default="./config/ml/sample-data-settings.dlm")

args = parser.parse_args()

def read_settings(file_name):
  settings = {}    

  with open(args.settings_file,"r+") as csv_file_in:
    file_reader = csv.reader(csv_file_in, delimiter='|')
    
    for row in file_reader:
      settings[row[0]] = row[1] 
  
  csv_file_in.close()

  #--- Map header names to indexes
  header = []
  with open(settings["SALES_FILE"],"r+") as sales_file_in:
    sales_file_reader = csv.reader(sales_file_in, delimiter=str(settings["DELIMITER"]))
    
    header = sales_file_reader.next()
  
  sales_file_in.close()

  # Customer dimension
  try:
    settings["CUSTOMER_COLUMN"] = header.index(settings["CUSTOMER_COLUMN_HEADER"])
  except ValueError:
    print "--- Customer data not present : adding default value"
    settings["CUSTOMER_COLUMN"] = -1 

  # Product dimension
  try:
    product_columns = settings["PRODUCT_COLUMN_HEADER"].rsplit(",")    
    product_indexes = []
    
    for pc in product_columns:
      i = header.index(pc)
      product_indexes.append(str(i))
    
    settings["PRODUCT_COLUMN"] = ','.join(product_indexes)    
  except ValueError:
    print "--- Product data not present : adding default value"
    settings["PRODUCT_COLUMN"] = -1 

  # Location dimension  
  try:
    settings["LOCATION_COLUMN"] = header.index(settings["LOCATION_COLUMN_HEADER"])
  except ValueError:
    print "--- Location data not present : adding default value"
    settings["LOCATION_COLUMN"] = -1 

  # Calendar dimension
  try:
    settings["CALENDAR_COLUMN"] = header.index(settings["CALENDAR_COLUMN_HEADER"])
  except ValueError:
    print "ERROR : CALENDAR column must be present"
    exit(1)

  # Target
  try:
    settings["TARGET_COLUMN"] = header.index(settings["TARGET_COLUMN_HEADER"])
  except ValueError:
    print "ERROR : TARGET column must be present"
    exit(1)
  
  return settings

def get_customer(settings,row):
  customer = "CS_1"
  
  column = int(settings["CUSTOMER_COLUMN"])
  if column >= 0:
    customer = row[column]

  return customer

def get_product(settings,row):
  product = "PROD_1"
  
  columns_list = str(settings["PRODUCT_COLUMN"])
  columns = columns_list.rsplit(",")
  if int(columns[0]) >= 0:
    products = []
    for c in columns:
      if c >= 0:
        products.append(row[int(c)])
    
    product = '-'.join(products)    
        
  return product
  
def get_location(settings,row):
  location = "LOC_1"
  
  column = int(settings["LOCATION_COLUMN"])
  if column >= 0:
    location = row[column]

  return location

def get_calendar(settings,row):
  column = int(settings["CALENDAR_COLUMN"])
  if column >= 0:
    calendar = row[column]
  else : 
    print "ERROR : CALENDAR_COLUMN cannot be negative."
    exit(1)

  return calendar

def get_target(settings,row):
  column = int(settings["TARGET_COLUMN"])
  if column >= 0:
    target = row[column]
  else : 
    print "ERROR : TARGET_COLUMN cannot be negative."
    exit(1)

  return target

def generate_sales_file(settings):
  with open(settings["SALES_FILE"],"r+") as sales_file_in:
    sales_file_reader = csv.reader(sales_file_in, delimiter=str(settings["DELIMITER"]))
    
    sales_file_out = open(settings["FOLDER"] + "/sales.dat","w")
    sales_file_out.writelines("CUSTOMER_SEGMENT|PRODUCT|LOCATION|CALENDAR|SALES\n")
  
    #--- Skip header, process each input row, and create the output one
    next(sales_file_reader,None)
    for row in sales_file_reader:
      l  = ""
      l += get_customer(settings,row) + "|"
      l += get_product(settings,row) + "|"  
      l += get_location(settings,row) + "|"
      l += get_calendar(settings,row) + "|"
      l += get_target(settings, row) + "\n"
      sales_file_out.writelines(l)
      
    sales_file_out.close()
      
  sales_file_in.close()
   
  return 0
   
def generate_customer_file(settings):
  with open(settings["SALES_FILE"],"r+") as sales_file_in:
    sales_file_reader = csv.reader(sales_file_in, delimiter=str(settings["DELIMITER"]))

    customers = set()
    next(sales_file_reader,None)
    for row in sales_file_reader:
      customers.add(get_customer(settings,row))

    customer_file_out = open(settings["FOLDER"] + "/customer.dat","w")
    customer_file_out.writelines("CUSTOMER_SEGMENT|CUSTOMER_SEGMENT_LABEL\n")
    
    for x in set(customers):
      l = x + "|" + x + "\n"
      customer_file_out.writelines(l)

    customer_file_out.close()
            
  sales_file_in.close()
     
  return 0   
  
def generate_product_file(settings):
  with open(settings["SALES_FILE"],"r+") as sales_file_in:
    sales_file_reader = csv.reader(sales_file_in, delimiter=str(settings["DELIMITER"]))

    products = set()
    next(sales_file_reader,None)
    for row in sales_file_reader:
      products.add(get_product(settings,row))

    product_file_out = open(settings["FOLDER"] + "/product.dat","w")
    product_file_out.writelines("PRODUCT|PRODUCT_LABEL\n")
    
    for x in set(products):
      l = x + "|" + x + "\n"
      product_file_out.writelines(l)

    product_file_out.close()
            
  sales_file_in.close()
     
  return 0   

def generate_location_file(settings):
  with open(settings["SALES_FILE"],"r+") as sales_file_in:
    sales_file_reader = csv.reader(sales_file_in, delimiter=str(settings["DELIMITER"]))

    locations = set()
    next(sales_file_reader,None)
    for row in sales_file_reader:
      locations.add(get_location(settings,row))

    location_file_out = open(settings["FOLDER"] + "/location.dat","w")
    location_file_out.writelines("LOCATION|LOCATION_LABEL\n")
    
    for x in set(locations):
      l = x + "|" + x + "\n"
      location_file_out.writelines(l)

    location_file_out.close()
            
  sales_file_in.close()
     
  return 0   

def main():
  settings = read_settings(args.settings_file)
  
  generate_sales_file(settings)
  generate_customer_file(settings)
  generate_product_file(settings)
  generate_location_file(settings)
  
  return 0


if __name__ == "__main__":
  main()
    

#! /usr/bin/env bash

export RC_SUCCESS=0
export RC_FAILURE=1

sUsage='
Usage: create-partition-settings-file.sh <partition> <input master-config file> <output settings-file >
 This script accepts 3 arguments: 
 - the partition name
 - the input master config file (containing high level run and fama related settings)
 - the output settings file file
 The master config file is expected to have a list of partitions 
  as well as a default one that will be used for any partition that
  that is not listed in the file.
'

#--- Check the number of arguments to the script
if [ "$#" -lt "3" ]
then
  echo
  echo "ERROR: Wrong number of arguments
  ${sUsage}" >&2
  exit ${RC_FAILURE}
fi

#--- Reset the arguments
partition=""
master_config=""
output_file=""

input_partition=${1}
master_config=${2}
output_file=${3}
partition=${input_partition}

#--- Check for input file
if [[ ! -f ${master_config} ]]; then
  echo "ERROR: $master_config does not exist. Stopping..."
  echo
  echo "${sUsage}" >&2
  exit ${RC_FAILURE}
fi

#--- Get the header from the master-config file and the count of fields
col_ids=$(head -1 ${master_config})
col_ids_count=$(echo ${col_ids} | awk -F"|" '{print NF}')

#--- Check for multiple entries of the same partition & use "default" if none found
if [[ $(grep -w $partition ${master_config} | wc -l) -gt 1 ]]; then
  echo "ERROR: Multiples lines for ${partition} in ${master_config}. Exiting..."
  exit ${RC_FAILURE}
else 
  if [[ $(grep -w ${partition} ${master_config} | wc -l) -ne 1 ]]; then
    echo "Partition is not found in ${master_config}. default settings will be used."
    partition="default"
  fi
  
  # Set variable to the line for the partition
  partition_params=$(grep -w ${partition} ${master_config})
  
  # If the parameters are still empty (which likely means the 'default' line is not found), then exit
  if [[ ${partition_params} == "" ]]; then
    echo "ERROR: Partition ${partition} is not found in ${master_config}. Exiting..."
    exit ${RC_FAILURE}
  else
    echo "Parameters from $master_config are ${partition_params}"
    partition_params_count=$(echo ${partition_params} | awk -F"|" '{print NF}')
  fi
fi

#--- Ensure the number of column IDs match the number of fields in the parameters for the partition row
if [[ ${col_ids_count} -ne ${partition_params_count} ]]; then
  echo "ERROR: The number of header columns and partition parameter columns in ${master_config} does not match. Exiting..."
  exit ${RC_FAILURE}
fi

#--- Write the initial line to a new file before writing the rest of the output
echo "PARTITION|${input_partition}" > ${output_file}

#--- Loop through the col_ids and partition_params to build the output file
i=2
while [[ $i -le ${col_ids_count} ]]; do 
  field_id=$(echo ${col_ids} | cut -d"|" -f $i)
  param_value=$(echo ${partition_params} | cut -d"|" -f $i)
  
  # Write the parameter to the output file only if it is populated
  if [[ ! ${param_value} == "" ]]; then
    echo "${field_id}|${param_value}" >> ${output_file} 
  fi
  
  i=$((i+1)); 
done

exit ${RC_SUCCESS}

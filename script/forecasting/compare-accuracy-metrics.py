import sys
import csv

# Usage : compares FAMA from run_2 against FAMA from run_1
#  python script/forecasting/compare_product_accuracy_metrics.py sku_accuracy_metrics_1.csv sku_accuracy_metrics_2.csv threshold  


def read_two_runs(filename_1,filename_2,ki,wi,bi):

  fama_1_wape_dict = {}
  fama_1_bias_dict = {}
  fama_2_wape_dict = {}
  fama_2_bias_dict = {}
  
  with open(filename_1,'r') as f_1:
    reader_1 = csv.reader(f_1,delimiter="|")
    reader_1.next()

    for row in reader_1:
      p = row[ki]
      if row[wi]: 
        fama_1_wape_dict[p] = float(row[wi])
      if row[bi]: 
        fama_1_bias_dict[p] = float(row[bi])

  with open(filename_2,'r') as f_2:
    reader_2 = csv.reader(f_2,delimiter="|")
    reader_2.next()

    for row in reader_2:
      p = row[ki]
      if row[wi]: 
        fama_2_wape_dict[p] = float(row[wi])
      if row[bi]: 
        fama_2_bias_dict[p] = float(row[bi])
        
  return (fama_1_wape_dict,fama_1_bias_dict,fama_2_wape_dict,fama_2_bias_dict)

 
def compare(run_1_wape_dict,run_2_wape_dict,run_1_bias_dict,run_2_bias_dict,threshold):

  wape_improvement_count=0
  wape_degradation_count=0
  bias_improvement_count=0
  bias_degradation_count=0
	
  for key,wape1 in run_1_wape_dict.iteritems():
    if run_2_wape_dict.has_key(key):
      wape2 = run_2_wape_dict[key]
      if (wape1 - wape2) > int(threshold) :
        wape_improvement_count+=1
        print "  Improved product wape %s : wape1=%s - wape2=%s"%(key,wape1,wape2)
      elif (wape2 - wape1) > int(threshold) :
        wape_degradation_count+=1
        print "  Degraded product wape %s : wape1=%s - wape2=%s"%(key,wape1,wape2)
    else:
      print "  Key %s doesn't exist in run_2_wape_dict"%key

  for key,bias1 in run_1_bias_dict.iteritems():                                                      
    if run_2_bias_dict.has_key(key): 
      bias2 = run_2_bias_dict[key]                                                       
      if (abs(bias1) - abs(bias2)) > int(threshold) :                               
        bias_improvement_count+=1                                                  
        print "  Improved product bias %s : bias1=%s - bias2=%s"%(key,bias1,bias2)     
      elif (abs(bias2) - abs(bias1)) > int(threshold) :                        
        bias_degradation_count+=1                                             
        print "  Degraded product bias %s : bias1=%s - bias2=%s"%(key,bias1,bias2)
    else:                                                                            
        print "  Key %s doesn't exist in run_2_dict"%key      
        
  print "\n"      
  print "  Summary:"      
  print "    Number of WAPE improvements : %s"%wape_improvement_count
  print "    Number of WAPE degradations : %s"%wape_degradation_count
  print "    Number of BIAS improvements : %s"%bias_improvement_count                                         
  print "    Number of BIAS degradations : %s"%bias_degradation_count


def main():
  
  if len(sys.argv) == 7:
    print "FAMA run 2 versus FAMA run 1"
    run_1_file = sys.argv[1]
    run_2_file = sys.argv[2]
    threshold = sys.argv[3]
    ki=int(sys.argv[4])
    wi=int(sys.argv[5])
    bi=int(sys.argv[6])

    print "Threshold : %s"%threshold
    (fama_1_wape_dict,fama_1_bias_dict,fama_2_wape_dict,fama_2_bias_dict) = read_two_runs(run_1_file,run_2_file,ki,wi,bi)
    compare(fama_1_wape_dict,fama_2_wape_dict,fama_1_bias_dict,fama_2_bias_dict,threshold)
  else:
    print "Not a proper number of arguments. Usage with one or two run (scenario) files "
    print " python script/forecasting/compare_product_accuracy_metrics.py run_file threshold"
    print " python script/forecasting/compare_product_accuracy_metrics.py run_1_file run_2_file threshold"      

if __name__ == '__main__':
	main()

#!/usr/bin/env bash

set -e

#-- Inputs
folder_exp=${1}
tag=${2}
partitions_file=${3}

#-- First partition
first_partition=$(head -1 ${partitions_file})

#-- Files to be created
prod_accuracy_metrics=${folder_exp}/"${tag}"_prod_accuracy_metrics.csv
overall_accuracy_metrics=${folder_exp}/"${tag}"_overall_accuracy_metrics.csv
per_partition_overall_accuracy_metrics=${folder_exp}/"${tag}"_per_partition_overall_accuracy_metrics.csv
tableau_report=${folder_exp}/"${tag}"_tableau_report.csv

function create_sku_accuracy_metrics()
{
  echo "--- Creating combined prod accuracy metrics" 
 
  #-- get header info 
  header=$(head -1 ${folder_exp}/${first_partition}/prod_accuracy_metrics.csv)

  echo ${header} > ${prod_accuracy_metrics}

  cat ${partitions_file} | while read partition 
  do
    sed "1d" ${folder_exp}/${partition}/prod_accuracy_metrics.csv >> ${prod_accuracy_metrics} 2>/dev/null || : 
  done  
}

function create_overall_accuracy_metrics()
{  
  echo "--- Creating combined overall accuracy metrics"  

  header="EXPERIMENT|TYPE|OVERALL_WAPE(PROD/LOC/CAL)|OVERALL_BIAS"
  echo ${header} > ${overall_accuracy_metrics}

  # Compute overall numbers 
  actuals=$(cut -d "|" -f 4 ${prod_accuracy_metrics} | awk 'BEGIN {bsl_sum=0} {bsl_sum+=$1} END{printf "%f", bsl_sum}')
  forecast=$(cut -d "|" -f 5 ${prod_accuracy_metrics} | awk 'BEGIN {bsl_sum=0} {bsl_sum+=$1} END{printf "%f", bsl_sum}')

  abs_err_prod_loc_cal=$(cut -d "|" -f 8 ${prod_accuracy_metrics} | awk 'BEGIN {bsl_sum=0} {bsl_sum+=$1} END{printf "%f", bsl_sum}')
   
  err=$(bc -l <<< "$forecast - $actuals")
  wape_prod_loc_cal=$(bc -l <<< "100.0*($abs_err_prod_loc_cal/$actuals)")
  bias=$(bc -l <<< "100.0*($err/$actuals)")
  
  echo "$tag|All|$wape_prod_loc_cal|$bias" >> ${overall_accuracy_metrics}
}

function create_per_partition_overall_accuracy_metrics()
{   
  echo "--- Creating per partition overall accuracy metrics"  
  
  header="EXPERIMENT|PARTITION|WAPE(PROD/LOC/CAL)|BIAS"
  echo ${header} > ${per_partition_overall_accuracy_metrics}
  
  cat ${partitions_file} | while read partition 
  do
    overall_metrics=$(sed "1d" ${folder_exp}/${partition}/overall_accuracy_metrics.csv | cut -d "|" -f 3-4) 
    if [ "$overall_metrics" != "" ]
    then
      echo "$tag|$partition|$overall_metrics" >> ${per_partition_overall_accuracy_metrics}
    fi
  done
}

function create_tableau_report()
{
  echo "--- Creating combined tableau report" 

  #-- get header info 
  header=$(head -1 ${folder_exp}/${first_partition}/tableau_report.csv)

  echo ${header} > ${tableau_report}

  cat ${partitions_file} | while read partition 
  do
     sed "1d" ${folder_exp}/${partition}/tableau_report.csv >> ${tableau_report} 2>/dev/null || : 
  done  
}

main()
{   
  create_sku_accuracy_metrics
  create_overall_accuracy_metrics
  create_per_partition_overall_accuracy_metrics

  create_tableau_report
}

main

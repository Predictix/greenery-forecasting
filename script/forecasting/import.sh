#!/usr/bin/env bash

#--- Define workspace name and import it
ws_name=${1}

lb web-server load -c out/lb-web-server.config

mkdir -p out/workspaces/${ws_name}
tar xvfz out/workspaces/demand-forecast-sample.tgz -C out/workspaces/${ws_name}
lb import-workspace ${ws_name} out/workspaces/${ws_name} --overwrite

lb exec ${ws_name} '^meta:workspaces:me[]="/'${ws_name}'".'
lb exec ${ws_name} '^dft_features_extract:features_config:set_service_prefix[]=meta:workspaces:me[].'
lb web-server load-services -w ${ws_name}


#--- Set data folders path & import data
data_dir=${PWD}/data/greenery
infor_dir=${PWD}/data/infor

log_dir=${PWD}/data/log/
mkdir -p $log_dir

echo "--- Importing product file"
time lb web-client import http://localhost:8080/${ws_name}/product \
  -i file://${data_dir}/final_full_greenery_poc_data.dlm -p -n -T 28800 > ${log_dir}/product.log 2>&1

# echo "--- Importing the prod desc file"
# time lb web-client import http://localhost:8080/${ws_name}/dft_prod_desc \
#   -i file://${data_dir}/product_desc.dat -p -n -T 28800 > ${log_dir}/product_desc.log 2>&1

echo "--- Importing location file"
time lb web-client import http://localhost:8080/${ws_name}/location \
  -i file://${data_dir}/final_full_greenery_poc_data.dlm -p -n -T 28800 > ${log_dir}/location.log 2>&1
      
echo "--- Importing holidays file"
time lb web-client import http://localhost:8080/${ws_name}/holidays \
  -i file://${infor_dir}/holidays.dat -p -n -T 28800 > ${log_dir}/holidays.log 2>&1

echo "--- Importing the sales file"
time lb web-client import http://localhost:8080/${ws_name}/sales \
  -i file://${data_dir}/final_full_greenery_poc_data.dlm -p -n -T 28800 > ${log_dir}/sales.log 2>&1


echo "--- Importing the promo file"
time lb web-client import http://localhost:8080/${ws_name}/promo \
  -i file://${data_dir}/promo_greenery.dlm -p -n -T 28800 > ${log_dir}/promo.log 2>&1


echo "--- Importing the jart to crop multiplier file"
time lb web-client import http://localhost:8080/${ws_name}/attributes \
  -i file://${data_dir}/jart_to_crop_multiplier.dlm -p -n -T 28800 > ${log_dir}/jart_to_crop_multiplier.log 2>&1

echo "--- Importing the promo lag weight file"
time lb web-client import http://localhost:8080/${ws_name}/promo_lag_weight \
  -i file://${data_dir}/crop_promo-prefix_start-date-lag_weight.dlm -p -n -T 28800 > ${log_dir}/promo_lag_weight.log 2>&1

echo "--- Importing SSA features file"
time lb web-client import http://localhost:8080/${ws_name}/ssa_features \
  -i file://${data_dir}/ssa_features.dlm -p -n -T 28800 > ${log_dir}/promo_lag_weight.log 2>&1


#! /usr/bin/env bash

set -e
set -x

PARTITION=${1}
WS_NAME=${2}
RUN_SETTINGS_FILE=${3}
MODEL_SETTINGS_FILE=${4}
INCLUDED_ATTRIBUTES_FILE=${5}
INCLUDED_BASELINE_ATTRIBUTES_FILE=${6}
EXCLUDED_FEATURES_FILE=${7}

#--- Read all relevant parameter settings 
get-param()
{
  value=$(grep -w ${1} ${2} | cut -d\| -f2 )
  echo ${value}
}

FORECAST_SETTINGS_FILE=$(get-param "FORECAST_SETTINGS_FILE" ${RUN_SETTINGS_FILE})
MODEL=$(get-param "MODEL" ${RUN_SETTINGS_FILE})
MODEL_NAME=$(get-param "MODEL_NAME" ${RUN_SETTINGS_FILE})
SAMPLING_FRACTION=$(get-param "SAMPLING_FRACTION" ${RUN_SETTINGS_FILE})
START_LEARNING=$(get-param "START_LEARNING_TYPE" ${RUN_SETTINGS_FILE})
INTERSECTION=$(get-param "INTERSECTION" ${RUN_SETTINGS_FILE})

ORDER=$(get-param "ORDER" ${MODEL_SETTINGS_FILE})

#--- Set up run folder structure
export EXECUTION_HOME=${PWD}
export FEATURE_EXT_HOME=${EXECUTION_HOME}/export/${PARTITION}
export LWFM_PATH=${EXECUTION_HOME}/upstream/foula/bin
mkdir -p ${FEATURE_EXT_HOME}

function set_forecast_domain()
{
  echo "--- Importing forecasting settings"
  time lb web-client import http://localhost:8080/${WS_NAME}/dft_forecast_domain_settings \
    -i file://${EXECUTION_HOME}/${FORECAST_SETTINGS_FILE} -p -n -T 28800   
}

function production_run()
{
  input_file=${EXECUTION_HOME}/${FORECAST_SETTINGS_FILE}
  value=$(awk 'BEGIN { FS = OFS = "|" } 
  { 
    if ($4 != "PRODUCTION_RUN") print $4
  }' ${EXECUTION_HOME}/${FORECAST_SETTINGS_FILE})

  echo ${value}
}

function weight-control()
{
  with_weight=-1
  sf=$(echo "${SAMPLING_FRACTION} 1.0" | awk '{if ($1 < $2) print "0"}')

  if [ "${sf}" == "0" ] ; then
      with_weight=0
  fi
  echo ${with_weight}
}

function export_train_data()
{
  #--- Note: we export traing set for -ALL- skus in the workspace!
  echo "--- Export the training set"
  echo '{"extract":"'${FEATURE_EXT_HOME}'/train.pb_0","is_train":true,"prod_rollup_label":"ALL","s3path":"/mnt/Data/train","upload":false, "header":"header,meta:3,sparse:double:%feat_count,","zero_sampling_fraction":'${SAMPLING_FRACTION}'}'| lb web-client call http://localhost:8080/${WS_NAME}/dft_sample_features -T 28800
}

function export_test_data()
{
  #--- Note: we export traing set for -ALL- skus in the workspace!
  echo "--- Export the test set"
  echo '{"extract":"'${FEATURE_EXT_HOME}'/test.pb_0","is_train":false,"prod_rollup_label":"ALL","s3path":"/mnt/Data/train","upload":false, "header":"header,meta:3,sparse:double:%feat_count"}'| lb web-client call http://localhost:8080/${WS_NAME}/dft_sample_features -T 28800
}

function export_feature_dictionary()
{
  echo "--- Export the features dictionary"
  lb web-client export http://localhost:8080/${WS_NAME}/dft_features_dict \
    -T 28800 -n -o ${FEATURE_EXT_HOME}/features_dict.out
  
   mv ${FEATURE_EXT_HOME}/features_dict.out ${FEATURE_EXT_HOME}/attribute_index
}

function run_ml()
{
  echo "--- ML with setting: ${MODEL} ${SAMPLING_FRACTION} ${START_LEARNING} ${INTERSECTION}"

  echo "--- ML with included attributes file: ${INCLUDED_ATTRIBUTES_FILE}"
  cp ${EXECUTION_HOME}/${INCLUDED_ATTRIBUTES_FILE} ${FEATURE_EXT_HOME}/included_attributes  
  echo "--- ML with included baseline attributes file: ${INCLUDED_BASELINE_ATTRIBUTES_FILE}"
  cp ${EXECUTION_HOME}/${INCLUDED_BASELINE_ATTRIBUTES_FILE} ${FEATURE_EXT_HOME}/included_baseline_attributes  
  echo "--- ML with excluded features file: ${EXCLUDED_FEATURES_FILE}"  
  cp ${EXECUTION_HOME}/${EXCLUDED_FEATURES_FILE} ${FEATURE_EXT_HOME}/excluded_features
  echo "--- ML with model settings file: ${MODEL_SETTINGS_FILE}"  
  cp ${EXECUTION_HOME}/${MODEL_SETTINGS_FILE} ${FEATURE_EXT_HOME}/model_parameters.dlm  
  
  WITH_WEIGHT=$(weight-control)
  echo "--- ML with weight : ${WITH_WEIGHT}" 

  if [ "${START_LEARNING}" == "warm" ] ; then
    echo "--- Warm/Incremental learning: updating coefficients"
	  mkdir -p ${FEATURE_EXT_HOME}/warmup
    python ${DEMAND_MGMT_HOME}/script/revert-and-update-coeffs.py \
      --dictionary_file ${FEATURE_EXT_HOME}/features_dict.out \
      --input_folder ${EXECUTION_HOME}/warmup --output_folder ${FEATURE_EXT_HOME}/warmup
  fi
  
  bash ${DEMAND_MGMT_HOME}/script/${MODEL}.sh ${FEATURE_EXT_HOME} ${WITH_WEIGHT} ${START_LEARNING} ${INTERSECTION} > ${FEATURE_EXT_HOME}/lwfm.log
}

function run_tf() 
{
  echo "--- Run TF"

  echo "--- ML with excluded attributes file: ${INCLUDED_ATTRIBUTES_FILE}"
  cp ${EXECUTION_HOME}/${INCLUDED_ATTRIBUTES_FILE} ${FEATURE_EXT_HOME}/included_attributes
  echo "--- ML with excluded features file: ${EXCLUDED_FEATURES_FILE}"  
  cp ${EXECUTION_HOME}/${EXCLUDED_FEATURES_FILE} ${FEATURE_EXT_HOME}/excluded_features
    
  bash ${DEMAND_MGMT_HOME}/script/run-tf.sh ${DEMAND_MGMT_HOME}/script ${RUN_SETTINGS_FILE} ${FEATURE_EXT_HOME} ${START_LEARNING}
}

function import_lfama_coeffs()
{  
  echo "--- Import fitted (edb forecast)"
  lb web-client import http://localhost:55183/${WS_NAME}/${MODEL}/dft_ml_fitted \
    -i file://${FEATURE_EXT_HOME}/fitted.dlm -p -n -T 28800 

  echo "--- Import lfama coefficients (idb forecast)"
  
  MIN_FORECAST=$(get-param "MIN_FORECAST" ${MODEL_SETTINGS_FILE})
  lb exec ${WS_NAME} '^dft_forecasting:lfama:predict_forecast:min_forecast[]='${MIN_FORECAST}'.'

  if [ ! -f "${FEATURE_EXT_HOME}/coeffs2.dlm" ] ; then 
    echo "cluster|feature|index|value" > ${FEATURE_EXT_HOME}/coeffs2.dlm 
  fi
  if [ ! -f "${FEATURE_EXT_HOME}/coeffs3.dlm" ] ; then 
    echo "cluster|feature|index|value" > ${FEATURE_EXT_HOME}/coeffs3.dlm 
  fi
  if [ ! -f "${FEATURE_EXT_HOME}/coeffscentroid.dlm" ] ; then 
    echo "centroid|index|value" > ${FEATURE_EXT_HOME}/coeffscentroid.dlm 
  fi
 
  lb workflow run --workspace /workflow  --file src/workflow/ml/import-lfama-coeffs.wf --main main --param "workspace=${WS_NAME}" --param "ml_model=${MODEL}" --param "coeff_dir=${FEATURE_EXT_HOME}" --auto-terminate 1 --overwrite 
}

function import_multi_model_coeffs()
{ 
  echo "--- Import multi-model coefficients (idb forecast)"
  
  echo "prod|loc|model" > ${FEATURE_EXT_HOME}/mm_model_keyspace.dlm
  lb exec ${WS_NAME} '_(p,l,m) <- dft_feature_sampling:observations:observable(p,l,_), m="'${MODEL_NAME}'".' \
    --exclude-ids --delimiter "|" --csv  --print
  cat _.csv >> ${FEATURE_EXT_HOME}/mm_model_keyspace.dlm && rm _.csv

  coeffs_files="coeffs0.dlm coeffs1.dlm coeffs2.dlm coeffs3.dlm coeffscentroid.dlm feature_node.dlm" 
  for f in ${coeffs_files}
  do
    if [ "${f}" == "coeffs0.dlm" ] ; then
      echo "model|cluster|bias" > ${FEATURE_EXT_HOME}/mm_${f}
    elif [ "${f}" == "coeffs1.dlm" ] ; then
      echo "model|cluster|feature|value" > ${FEATURE_EXT_HOME}/mm_${f}
    elif [ "${f}" == "coeffs2.dlm" ] ; then
      echo "model|cluster|feature|index|value" > ${FEATURE_EXT_HOME}/mm_${f}
    elif [ "${f}" == "coeffs3.dlm" ] ; then
      echo "model|cluster|feature|index|value" > ${FEATURE_EXT_HOME}/mm_${f}
    elif [ "${f}" == "coeffscentroid.dlm" ] ; then
      echo "model|centroid|index|value" > ${FEATURE_EXT_HOME}/mm_${f}
    elif [ "${f}" == "feature_node.dlm" ] ; then
      echo "model|feature|snode|enode" > ${FEATURE_EXT_HOME}/mm_${f}
    fi

    if [ -e "${FEATURE_EXT_HOME}/${f}" ] ; then   
      tail -n+2 ${FEATURE_EXT_HOME}/${f} | awk 'BEGIN { FS = OFS = "|" } 
      { 
        print "'${MODEL_NAME}'|"$0
      }' >> ${FEATURE_EXT_HOME}/mm_${f}
    fi
  done

  echo "model|min_forecast" > ${FEATURE_EXT_HOME}/mm_lfama.dlm
  if [ "${MODEL}" == "lfama" ] ; then
    MIN_FORECAST=$(get-param "MIN_FORECAST" ${MODEL_SETTINGS_FILE})
    echo "${MODEL_NAME}|${MIN_FORECAST}" >> ${FEATURE_EXT_HOME}/mm_lfama.dlm
  fi

  echo "model|sales_ranges|telescoping|classifier_objective|min_forecast" > ${FEATURE_EXT_HOME}/mm_sfama.dlm
  if [ "${MODEL}" == "sfama" ] ; then
    SALES_RANGES=$(get-param "SALES_RANGES" ${MODEL_SETTINGS_FILE})
    TELESCOPING=$(get-param "TELESCOPING" ${MODEL_SETTINGS_FILE})
    CLASSIFIER_OBJECTIVE=$(get-param "CLASSIFIER_OBJECTIVE" ${MODEL_SETTINGS_FILE})
    MIN_FORECAST=$(get-param "MIN_FORECAST" ${MODEL_SETTINGS_FILE})
    echo "${MODEL_NAME}|${SALES_RANGES}|${TELESCOPING}|${CLASSIFIER_OBJECTIVE}|${MIN_FORECAST}" >> ${FEATURE_EXT_HOME}/mm_sfama.dlm
  fi

  echo "model|max_depth|min_forecast" > ${FEATURE_EXT_HOME}/mm_dfama.dlm
  if [ "${MODEL}" == "dfama" ] ; then
    MIN_FORECAST=$(get-param "MIN_FORECAST" ${MODEL_SETTINGS_FILE})
    MAX_DEPTH=$(get-param "MAXDEPTH" ${MODEL_SETTINGS_FILE})
    echo "${MODEL_NAME}|${MAX_DEPTH}|${MIN_FORECAST}" >> ${FEATURE_EXT_HOME}/mm_dfama.dlm
  fi
 
  lb workflow run --workspace /workflow  --file src/workflow/ml/import-multi-model-coeffs.wf --main main --param "workspace=${WS_NAME}" --param "coeff_dir=${FEATURE_EXT_HOME}" --auto-terminate 1 --overwrite 

  if [[ $(wc -l ${FEATURE_EXT_HOME}/included_baseline_attributes | awk '{print $1}') -ge 2 ]]  
  then
    lb workflow run --workspace /workflow  --file src/workflow/ml/import-baseline-attributes.wf --main main --param "workspace=${WS_NAME}" --param "coeff_dir=${FEATURE_EXT_HOME}" --auto-terminate 1 --overwrite 
  fi

}

function import_ml_forecast()
{    
  echo "--- Import fitted & forecast (edb)"

  lb web-client import http://localhost:55183/${WS_NAME}/${MODEL}/dft_ml_fitted \
    -i file://${FEATURE_EXT_HOME}/fitted.dlm -p -n -T 28800 
    
  lb web-client import http://localhost:55183/${WS_NAME}/${MODEL}/dft_ml_forecast \
    -i file://${FEATURE_EXT_HOME}/total.dlm -p -n -T 28800 
}

function import_tf_forecast()
{    
  echo "--- Import fitted & forecast"

  #--- fitted
  lb web-client import http://localhost:55183/${WS_NAME}/${MODEL}/dft_ml_fitted \
    -i file://${FEATURE_EXT_HOME}/train-y.tableau -p -n -T 28800 
    
  #--- forecast
  lb web-client import http://localhost:55183/${WS_NAME}/${MODEL}/dft_ml_forecast \
    -i file://${FEATURE_EXT_HOME}/test-y.tableau -p -n -T 28800 
}

function export_forecast()
{
  echo "--- Export forecast"
  lb web-client export http://localhost:8080/${WS_NAME}/dft_forecast_export?tdx_file_mode="noquotes" \
    -n -T 14400 -o file://${FEATURE_EXT_HOME}/total_forecast.csv
}

function export_accuracy_metrics()
{
  echo "--- Export accuracy metrics"
  lb web-client export http://localhost:8080/${WS_NAME}/prod_accuracy_metrics \
    -n -T 14400 -o ${FEATURE_EXT_HOME}/prod_accuracy_metrics.csv
  lb web-client export http://localhost:8080/${WS_NAME}/location_accuracy_metrics \
    -n -T 14400 -o ${FEATURE_EXT_HOME}/location_accuracy_metrics.csv
  lb web-client export http://localhost:8080/${WS_NAME}/overall_accuracy_metrics \
    -n -T 14400 -o ${FEATURE_EXT_HOME}/overall_accuracy_metrics.csv
}

function export_tableau_reports()
{    
  echo "--- Export the tableau report"
  lb web-client export http://localhost:8080/${WS_NAME}/tableau \
    -n -T 14400 -o file://${FEATURE_EXT_HOME}/tableau_report.csv
}


main()
{ 
  #--- Set up & run ML
  time set_forecast_domain
  time export_feature_dictionary

  #--- Export, train & import coeffs/forecasts
  time export_train_data
  time export_test_data

  if [ "${MODEL}" == "tf" ]; then
    time run_tf
    time import_tf_forecast
  else
    time run_ml
    time import_ml_forecast
  fi

  #--- Exports
  time export_forecast
  time export_accuracy_metrics
  time export_tableau_reports
}

main

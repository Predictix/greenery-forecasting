#! /usr/bin/env bash

set -e

partition_name=${1}
workspace_name=${2}

get-param()
{
  value=$(grep -w ${1} ${2} | cut -d\| -f2 )
  echo ${value}
}

#--- Format sample sales data
mkdir -p data/data/
# python script/forecasting/format-data.py --settings_file config/forecasting/input-data-settings.dlm
# python upstream/demand-mgmt/script/forecasting/get_words.py --prod_file data/data/product.dat --out_file data/data/product_desc.dat --prod_column 0 --prod_label_column 1 --min_incidence 0

#--- Import data
bash script/forecasting/import.sh ${workspace_name}

#--- Generate eligibilities
#bash script/forecasting/orchestrate-eligibility.sh \
#  ${partition_name} ${workspace_name} config/forecasting/eligibility-settings.dlm

#--- Create partition run, model settings, included attributes files and excluded features files
bash script/forecasting/create-partition-settings-file.sh \
  ${partition_name} config/forecasting/master-settings.dlm partition-run-settings.dlm

MODEL_SETTINGS_FILE=$(get-param "MODEL_SETTINGS_FILE" partition-run-settings.dlm)
bash script/forecasting/create-partition-settings-file.sh \
  ${partition_name} ${MODEL_SETTINGS_FILE} partition-model-settings.dlm

INCLUDED_ATTRIBUTES_FILE=$(get-param "INCLUDED_ATTRIBUTES_FILE" partition-run-settings.dlm)
bash script/forecasting/create-partition-attributes-file.sh \
  ${partition_name} ${INCLUDED_ATTRIBUTES_FILE} partition-included-attributes.dlm

cp config/forecasting/included-baseline-attributes.dlm partition-included-baseline-attributes.dlm

EXCLUDED_FEATURES_FILE=$(get-param "EXCLUDED_FEATURES_FILE" partition-run-settings.dlm)
bash script/forecasting/create-partition-attributes-file.sh \
  ${partition_name} ${EXCLUDED_FEATURES_FILE} partition-excluded-features.dlm

#--- Generate forecasts
bash script/forecasting/orchestrate-ml.sh \
  ${partition_name} ${workspace_name} \
  partition-run-settings.dlm partition-model-settings.dlm \
  partition-included-attributes.dlm partition-included-baseline-attributes.dlm partition-excluded-features.dlm 

#--- Clean up
rm -f partition-run-settings.dlm
rm -f partition-model-settings.dlm
rm -f partition-included-attributes.dlm
rm -f partition-included-baseline-attributes.dlm
rm -f partition-excluded-features.dlm

rm .pid* 

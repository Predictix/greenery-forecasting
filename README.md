# README #

This repository contains the source code of demand forecasting sample application. Is a template that
can used to implement customer specific forecasting applications. At its core is based on forecasting
libraries from demand-mgmt repo (https://bitbucket.org/logicblox/demand-mgmt). By feeding a sales file
you can immediately generate an ML forecast.

# Install upstream dependencies & environment set up

Before you can build the app, you need to download the application dependencies. To do so, execute:

    ./script/get-dev-deps

An upsteam/ folder will be crated, and it will contain the compiled and installed code of various
dependencies

- logicblox, foula
- retail-model (basic retail entities)
- demand-mgmt (forecasting libraries)

Additionally, for running Tensor Flow Neural Network models, following packages need to be installed in Python 2.7 (they are prerequisite, not installed by get-dev-deps):

- tensorflow
- configparser
- pandas
- numpy

To set up your environment:

    source ./script/env.sh

After setting up environment, you have to restart LogicBlox:

    lb services restart

# Build the project & install

To perform a full build from scratch:

    make spotless && lb config && make && make install

The compiled code will be installed under `out` folder. 

To generate forecasting features, perform these steps -- needs updates

- Compile project as shown above
- Install workspace by executing `install.sh` inside `out` folder
- Execute this command in repository root:
    ```
    ./upstream/model-refinery/bin/model-refinery featureGen src/logiql/base custom_generated_features config/model-refinery/features-config.csv /demand-forecast-sample
    ```
- After the code is generated, in `config.py`, add `custom_generated_features` to `custom_base_modules` array
- Configure and compile project again

# Configuration files -- needs updates

The main configuration files are:

    config/forecasting/orchestrate-settings.dlm
    config/forecasting/ml/ml-parameters.dlm (for running lwfm methods)
    config/forecasting/tf/experiment00.cfg (for running Tensor Flow methods)

Example orchestrate-settings.dlm for lwfm methods:

    SETTING|VALUE
    FORECAST_SETTINGS_FILE|config/forecasting/forecast-domain-settings.dat
    EXCLUDED_ATTRIBUTES_FILE|config/forecasting/ml/excluded-attributes-default
    EXCLUDED_FEATURES_FILE|config/forecasting/ml/excluded-features-default
    MODEL|lfama
    MODEL_PARAMETERS_FILE|config/forecasting/ml/ml-parameters.dlm
    SAMPLING_FRACTION|1.0
    START_LEARNING_TYPE|cold

Example orchestrate-settings.dlm for TF methods:


    SETTING|VALUE
    FORECAST_SETTINGS_FILE|config/forecasting/forecast-domain-settings.dat
    EXCLUDED_ATTRIBUTES_FILE|config/forecasting/ml/excluded-attributes-default
    EXCLUDED_FEATURES_FILE|config/forecasting/ml/excluded-features-default
    MODEL|tf
    MODEL_PARAMETERS_FILE|config/forecasting/tf/experiment00.cfg
    SAMPLING_FRACTION|1.0
    START_LEARNING_TYPE|cold

Note that for TF runs, besides MODEL and MODEL_PARAMETERS_FILE all other settings are irrelevant.

# Extract product description words from product labels

To get product description words from product labels, the following command should be manually run
before launching the end to end run. This will generate the file data/data/product_desc.dat which
will be subsequently imported in order to generate product_desc features.

    python upstream/demand-mgmt/script/forecasting/get_words.py --prod_file data/data/product.dat --out_file data/data/product_desc.dat --prod_column 0 --prod_label_column 1

# Get holiday data

To get product holiday data the following command should be manually run before launching the end to end run.
The script will use https://www.timeanddate.com api and will generate the file data/data/holiday.dat 
which will be subsequently imported in order to generate holiday features.

    python upstream/demand-mgmt/script/forecasting/get_holidays.py --country_file data/data/holiday_country_file.dat --out_file data/infor/holidays.dat --access_key acceskey --secret_key secretkey

where

* "country_file" is a file of the following format:

    COUNTRY|YEAR|TYPE
    us|2017|federal
    us|2018|federal

* "access_key" and "secret_key" are two keys required to access the API, which need to be provided by the project manager.

# Sample end-to-end run -- needs updates

To get a birds-eye-view of the overall process, you can run locally an end-to-end ML run
on some sample data.
There are two end-to-end ML run types:

1. Run from sales.dat

    bash script/forecasting/end-to-end-run.sh sample sample-ws

Firstly, the script will process a sample sales file (see config/forecasting/input-data-settings.dlm),
and create basic hierarchy and sales data under data/data/ folder. These files will be imported into the
the workspace called "sample-ws". Last step is to call the machine learning.

where

* "sample" is the data partition name (can be whatever)
* "sample-ws" is the workspace

2. Run from sales.dat, product.dat, and location.dat

    bash script/forecasting/end-to-end-run-full.sh sample sample-ws

Firstly, the script will copy the sample sales, product, and location files from data/sample onto the data/data 
folder. Then the file product_desc.dat containing product description words is generated by calling the get_words
python script on the product file. All these files will be imported into the workspace called "sample-ws". 
Last step is to call the machine learning similar to 1.

Below are some details regarding the settings for different parameters in the configuration file:

* FORECAST_SETTINGS_FILE : file containing the forecasting horizon settings. E.g. config/ml/forecast-domain-settings.dat
* EXCLUDED_ATTRIBUTES_FILE : file under config/ml/ that lists all attributes to be excluded during machine learning run for this partition. E.g excluded_attributes_default
* EXCLUDED_FEATURE_FILE : file under config/ml that lists all features to be excluded during machine learning run for this partition. E.g.  excluded_features_default
* MODEL : machine learning method to use. E.g. fama (Note that a corresponding script/fama.sh must exist that implements the model)
* MODEL_PARAMETERS_FILE : file under cofig/ml that specifies parameter settings to be used by the machine learning. E.g ml-parameters-default.dlm
* SAMPLING_FRACTION : sampling fraction to use. Usually we sample less that 100% for very large data sets. E.g. 1.0 (i.e. 100% sampling)
* START_LEARNING_TYPE : How to initialize the machine learning run: "cold" or "warm". "cold" indicates starting from a random initial solution, while "warm" indicates starting from a solution found in a previous run (AKA "incremental learning"). E.g. cold

The results of the above sample run (forecasts, reports, run details etc) can be found under
export/sample/ folder.


# On processing sales data & end-to-end local runs

Given some customer sales data, we'll first process it and also create some basic hierarchy files
in a format that is understood by our tdx services :

   python script/forecasting/format-data.py --settings_file config/forecasting/input-data-settings.dlm

Here is a sample config/forecasting/input-data-settings.dlm, along with some explanations :

    SETTING|VALUE
    FOLDER|data/data : where to save the processed data
    SALES_FILE|data/sample/sales.dat : input client sales data
    DELIMITER|"|" :
    CUSTOMER_COLUMN_HEADER|NA : name of the header in the input sales file that specifies customer segment
    PRODUCT_COLUMN_HEADER|PRODUCT : name of the header in the input sales file that specifies the product
    LOCATION_COLUMN_HEADER|LOCATION : name of the header in the input sales file that specifies the location
    CALENDAR_COLUMN_HEADER|CALENDAR : name of the header in the input sales file that specifies the dates
    TARGET_COLUMN_HEADER|SALES : name of the header in the input sales file that specifies unit sales


Then import the data into "sample-ws" workspaces

    bash script/forecasting/import.sh sample-ws

and then generate coefficients (forecasts, reports, etc)

    bash script/forecasting/orchestrate.sh sample sample-ws config/forecasting/orchestrate-settings.dlm

Results will be under export/sample/ folder.


# Sales data keyed by customer segment

You'll need first to implement the following projects in such a way one of the keys is the customer segment :

    dft_dims_tdx (customer segment segment service)
    dft_metrics_tdx (sales data keyed by customer segment)
    dft_reports_tdx (reports keyd by customer segment)
    dft_register_tdx

You'll need to add the projects to the config.py, as well as to dft_with_customer.project. To compile and install

    make dft-with-customer && make install

Before running an end to end loop, you'll need to update the import.sh (to import the customer segments),
excluded_attributes (to include customer segment related features), and orchestrate.sh (to call the correct protobuf
feature export service.)


# Spawning jobs with lb-steve

*** NEEDS UPDATE ***

The following input folder structure is expected by all jobs spawned with lb-steve. This folder structure can reside on s3 (this is usually the case) or can be local.

```
|-- data/
|   |-- infor/
|       |-- infor_temporal_attributes.dat
|   |-- partitions-1/
|       |-- sales.dat
|   |-- partitions-2/
|       |-- sales.dat
|   |-- ...
|   |-- sample/
|       |-- sales.dat
|-- lb-steve/
|   |-- deploy/
|       |-- out/
|       |-- upstream/
|           |-- foula/
|           |-- logicblox/
|-- output/
|   |-- dft-test/
|       |-- partition-1/
|       |-- partition-2/
|       |-- ...
|       |-- sample/
|   |-- ...
|-- warmup-coeffs/
|   |-- ...
|   |-- partition-1/
|       |-- coeffs/
|           |-- coeffs0
|           |-- coeffs1
|   |-- partition-2/
|       |-- coeffs/
|           |-- coeffs0
|           |-- coeffs1
|   |-- ...
|   |-- sample/
|       |-- coeffs/
|           |-- coeffs0
|           |-- coeffs1
```

Details regarding the folder's content:

* data/ : contains folders for each data partition; in addition calendar hierarchy is under a commonly used folder called infor/
* lb-steve/out/ : contains the project deployment (i.e. workspace, compiled code, scripts, configuration files, etc)
* lb-steve/upstream/ : contains project dependencies like foula/ release (i.e. machine learning related code)
* output/ : contains the results of various runs
* warmup-coeffs/ : contains folders for each relevant data partition with initial solution/coefficients to start the run with

## Update project deployment code

*** NEEDS UPDATE ***

In this step you'll need to compile and install the project, and then update the "out/" folder in s3 bucket. For this execute the following commands:

    make clean && lb config && make && make install

After this, the local out/ folder is up to date. Next step is to upload it to the relevant s3 folder

    aws --region=us-east-1 --acl=public-read-write s3 cp out s3://{your-project}/lb-steve/deploy/out --recursive


## Launching jobs

*** NEEDS UPDATE ***

You'll need credentials to access lb-steve, as well as lb-steve needs access to your s3 bucket.

First step is to upload the "implementation"

    lb-steve upload-impl --impl dft -i src/implementation/end-to-end

After all the preparation steps are done, you can launch any number of lb-steve jobs using the following call to an lb workflow

    lb workflow run -f src/workflow/ml/main-dft-ml.wf --overwrite --auto-terminate 1 --workspace /dft-workflow --param "partitions_to_run=onfig/partitions-to-run-default.dlm" --param "s3_folder=s3://{your-project}" --param "tag=dft-wf-sample-run" --param "user_name=your-name" --param "user_key=your-key.pem

where the meaning of the options/parameters are

* overwrite : indicates to override the workspace used by lb workflow (if one exists)
* auto-terminate : terminate the execution when workflow is done
* workspace : name of the workspace that's used by lb workflow
* param "partitions_to_run=onfig/partitions-to-run-default.dlm" : specifies the file to use to read the partitions to run, along with some settings associated with them.
* param "s3_folder=s3://{your-project} : is an s3 folder under which the folder structure as mentioned above can be found.
* param "tag=dft-wf-sample-run" : label to distinguish among different runs (potentially with different settings and/or different data sets)

To check the status of the jobs you can execute

    lb workflow status

or to find failed jobs you can execute

    lb workflow status --failed

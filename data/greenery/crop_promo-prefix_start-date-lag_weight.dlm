Gewas_crop|Promo_Type_Prefix|Delivery_Date_minus_Promo_Start_Day|Weight
Aardbei|Bulkpla|-7|0.169
Aardbei|Bulkpla|-6|0.148
Aardbei|Bulkpla|-5|0.076
Aardbei|Bulkpla|-4|0.212
Aardbei|Bulkpla|-3|0.24
Aardbei|Bulkpla|-2|0.155
Aardbei|Extra G|-9|0.24
Aardbei|Extra G|-8|0.091
Aardbei|Extra G|-7|0.095
Aardbei|Extra G|-6|0.127
Aardbei|Extra G|-5|0.216
Aardbei|Extra G|-4|0.23
Aardbei|Seizoen|-9|0.019
Aardbei|Seizoen|-8|0.021
Aardbei|Seizoen|-7|0.024
Aardbei|Seizoen|-6|0.034
Aardbei|Seizoen|-5|0.034
Aardbei|Seizoen|-4|0.025
Aardbei|Seizoen|-2|0.118
Aardbei|Seizoen|-1|0.111
Aardbei|Seizoen|0|0.124
Aardbei|Seizoen|1|0.162
Aardbei|Seizoen|2|0.18
Aardbei|Seizoen|3|0.148
Aardbei|Weekend|-11|0.018
Aardbei|Weekend|-10|0.019
Aardbei|Weekend|-9|0.044
Aardbei|Weekend|-8|0.053
Aardbei|Weekend|-7|0.035
Aardbei|Weekend|-6|0.03
Aardbei|Weekend|-5|0.013
Aardbei|Weekend|-4|0.055
Aardbei|Weekend|-3|0.063
Aardbei|Weekend|-2|0.09
Aardbei|Weekend|-1|0.433
Aardbei|Weekend|0|0.099
Aardbei|Weekend|1|0.048
Bloemkool|Begin v|-7|0.103
Bloemkool|Begin v|-6|0.096
Bloemkool|Begin v|-5|0.087
Bloemkool|Begin v|-4|0.079
Bloemkool|Begin v|-3|0.202
Bloemkool|Begin v|-2|0.287
Bloemkool|Begin v|0|0.034
Bloemkool|Begin v|1|0.036
Bloemkool|Begin v|2|0.035
Bloemkool|Begin v|3|0.018
Bloemkool|Begin v|4|0.01
Bloemkool|Begin v|5|0.012
Bloemkool|Bulkpla|-7|0.078
Bloemkool|Bulkpla|-6|0.083
Bloemkool|Bulkpla|-5|0.088
Bloemkool|Bulkpla|-4|0.111
Bloemkool|Bulkpla|-3|0.054
Bloemkool|Bulkpla|-2|0.057
Bloemkool|Bulkpla|0|0.083
Bloemkool|Bulkpla|1|0.075
Bloemkool|Bulkpla|2|0.093
Bloemkool|Bulkpla|3|0.096
Bloemkool|Bulkpla|4|0.106
Bloemkool|Bulkpla|5|0.075
Bloemkool|Extra G|-10|0.005
Bloemkool|Extra G|-9|0.123
Bloemkool|Extra G|-8|0.124
Bloemkool|Extra G|-7|0.115
Bloemkool|Extra G|-6|0.135
Bloemkool|Extra G|-5|0.118
Bloemkool|Extra G|-4|0.084
Bloemkool|Extra G|-2|0.039
Bloemkool|Extra G|-1|0.081
Bloemkool|Extra G|0|0.043
Bloemkool|Extra G|1|0.05
Bloemkool|Extra G|2|0.043
Bloemkool|Extra G|3|0.04
Bloemkool|Seizoen|-2|0.176
Bloemkool|Seizoen|-1|0.188
Bloemkool|Seizoen|0|0.145
Bloemkool|Seizoen|1|0.159
Bloemkool|Seizoen|2|0.162
Bloemkool|Seizoen|3|0.17
Bloemkool|Weekend|-11|0.112
Bloemkool|Weekend|-10|0.053
Bloemkool|Weekend|-9|0.356
Bloemkool|Weekend|-8|0.376
Bloemkool|Weekend|-7|0.033
Bloemkool|Weekend|-6|0.071
Meloen|Bulkpla|-7|0.114
Meloen|Bulkpla|-6|0.227
Meloen|Bulkpla|-5|0.154
Meloen|Bulkpla|-4|0.143
Meloen|Bulkpla|-3|0.207
Meloen|Bulkpla|-2|0.155
Meloen|Extra G|-9|0.08
Meloen|Extra G|-8|0.198
Meloen|Extra G|-7|0.141
Meloen|Extra G|-6|0.201
Meloen|Extra G|-5|0.206
Meloen|Extra G|-4|0.173
Meloen|Overig|-7|0.034
Meloen|Overig|-6|0.056
Meloen|Overig|-5|0.276
Meloen|Overig|-4|0.569
Meloen|Overig|-3|0.005
Meloen|Overig|-2|0.061
Meloen|Seizoen|-9|0.02
Meloen|Seizoen|-8|0.031
Meloen|Seizoen|-7|0.024
Meloen|Seizoen|-6|0.034
Meloen|Seizoen|-5|0.038
Meloen|Seizoen|-4|0.041
Meloen|Seizoen|-2|0.122
Meloen|Seizoen|-1|0.122
Meloen|Seizoen|0|0.117
Meloen|Seizoen|1|0.15
Meloen|Seizoen|2|0.157
Meloen|Seizoen|3|0.143
Meloen|Thema|-7|0.157
Meloen|Thema|-6|0.088
Meloen|Thema|-5|0.206
Meloen|Thema|-4|0.157
Meloen|Thema|-3|0.206
Meloen|Thema|-2|0.186
Meloen|Weekend|-11|0.043
Meloen|Weekend|-10|0.032
Meloen|Weekend|-9|0.232
Meloen|Weekend|-8|0.297
Meloen|Weekend|-7|0.031
Meloen|Weekend|-6|0.049
Meloen|Weekend|-4|0.026
Meloen|Weekend|-3|0.012
Meloen|Weekend|-2|0.086
Meloen|Weekend|-1|0.146
Meloen|Weekend|0|0.019
Meloen|Weekend|1|0.028
Nectarine|Extra G|-9|0.079
Nectarine|Extra G|-8|0.125
Nectarine|Extra G|-7|0.083
Nectarine|Extra G|-6|0.1
Nectarine|Extra G|-5|0.115
Nectarine|Extra G|-4|0.083
Nectarine|Extra G|-2|0.058
Nectarine|Extra G|-1|0.113
Nectarine|Extra G|0|0.062
Nectarine|Extra G|1|0.071
Nectarine|Extra G|2|0.04
Nectarine|Extra G|3|0.07
Nectarine|Seizoen|-9|0.04
Nectarine|Seizoen|-8|0.047
Nectarine|Seizoen|-7|0.035
Nectarine|Seizoen|-6|0.053
Nectarine|Seizoen|-5|0.055
Nectarine|Seizoen|-4|0.053
Nectarine|Seizoen|-2|0.085
Nectarine|Seizoen|-1|0.128
Nectarine|Seizoen|0|0.105
Nectarine|Seizoen|1|0.131
Nectarine|Seizoen|2|0.123
Nectarine|Seizoen|3|0.144
Nectarine|Weekend|-11|0.02
Nectarine|Weekend|-10|0.02
Nectarine|Weekend|-9|0.022
Nectarine|Weekend|-8|0.022
Nectarine|Weekend|-7|0.025
Nectarine|Weekend|-6|0.029
Nectarine|Weekend|-4|0.047
Nectarine|Weekend|-3|0.043
Nectarine|Weekend|-2|0.117
Nectarine|Weekend|-1|0.533
Nectarine|Weekend|0|0.064
Nectarine|Weekend|1|0.058
